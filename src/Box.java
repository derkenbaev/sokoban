
public class Box {
	private int row;
	private int col;
	
	public Box (int row, int col){
		this.row = row;
		this.col = col;
	}
	
	
	public void moveUp(){
		if (row > 0)
			row--;
	}
	
	

	public void moveDown(){
		if (row < 7)
			row++;
	}
	
	
	public void moveRight(){
		if (col < 7){
			col++;
		}
	}
	

	public void moveLeft(){
		if (col > 0){
			col--;
		}
	}


	public int getCol() {
		return col;
	}
	
	public int getRow() {
		return row;
	}
}
