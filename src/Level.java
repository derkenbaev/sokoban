import java.util.Scanner;


public class Level {
	private int height;
	private int width;
	private int robotRow;
	private int robotCol;
	private int bestSteps;
	
	
	public int getRobotRow() {
		return robotRow;
	}
	public int getBestSteps() {
		return bestSteps;
	}

	public int getRobotCol() {
		return robotCol;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public void setMaze(char[][] maze) {
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){
				this.maze[i][j] = maze[i][j];
			}
		}
	}

	private char [][] maze;
		
	public Level(Scanner scan) throws Exception {
		height = Integer.parseInt(scan.nextLine());
		width = Integer.parseInt(scan.nextLine());
		if (height < 1 || width < 1){
			throw new Exception ("Incorrect size of maze");
		}
		
		robotRow = Integer.parseInt(scan.nextLine());
		robotCol = Integer.parseInt(scan.nextLine());
		if (robotRow < 1 || robotCol < 1){
			throw new Exception ("Incorrect robot's coordinates");
		}
		
		
		maze = new char [height][width];
		
		for (int i = 0; i < height; i++){
			String s = scan.nextLine();
			if (s.length() != width){
				throw new Exception ("Incorrect maze's data");
			}			
			for (int j = 0; j < width; j++){
				maze[i][j] = s.charAt(j);
				if (maze[i][j] != 'E' && maze[i][j] != 'W' && maze[i][j] != 'B' && maze[i][j] != 'P' && maze[i][j] != 'R'){
					throw new Exception ("Incorrect maze's data");					
				}
			}
			
		}

		bestSteps = Integer.parseInt(scan.nextLine());
		if (bestSteps < 0){
			throw new Exception ("Incorrect value of number of best steps");
		}
		
		if (scan.hasNextLine()){
			throw new Exception ("Incorrect maze's data");
		}
		
	}

	public char getChar (int i, int j){
		return maze[i][j];
	}

	
	public boolean win(){
		for (int i = 0; i < height; i++){
			for (int j = 0; j < width; j++){
				if (maze[i][j] == 'B'){
					return false;
				}
			}
			
		}
		
		return true;
		
	}
	
	public boolean moveLeft(int i, int j) {
		if (maze[i][j] == 'W')
			return false;
		else if (maze[i][j] == 'E' || maze[i][j] == 'P')
			return true;
		
		else if (maze[i][j] == 'B' && (maze[i][j-1] == 'E' || maze[i][j-1] == 'P')){
			maze[i][j] = 'E';
			maze[i][j-1] = (maze[i][j-1] == 'E' ? 'B' : 'X');
			return true;
			
		}
		

		else if (maze[i][j] == 'X' && (maze[i][j-1] == 'E' || maze[i][j-1] == 'P')){
			maze[i][j] = 'P';
			maze[i][j-1] = (maze[i][j-1] == 'E' ? 'B' : 'X');
			return true;
		}
		
		
		return false;
	}

	public boolean moveRight(int i, int j) {
		if (maze[i][j] == 'W')
			return false;
		else if (maze[i][j] == 'E' || maze[i][j] == 'P')
			return true;
		
		else if (maze[i][j] == 'B' && (maze[i][j+1] == 'E' || maze[i][j+1] == 'P')){
			maze[i][j] = 'E';
			maze[i][j+1] = (maze[i][j+1] == 'E' ? 'B' : 'X');
			return true;
			
		}
		

		else if (maze[i][j] == 'X' && (maze[i][j+1] == 'E' || maze[i][j+1] == 'P')){
			maze[i][j] = 'P';
			maze[i][j+1] = (maze[i][j+1] == 'E' ? 'B' : 'X');
			return true;
		}
		
		
		return false;
	}

	public boolean moveUp(int i, int j) {
		if (maze[i][j] == 'W')
			return false;
		else if (maze[i][j] == 'E' || maze[i][j] == 'P')
			return true;
		
		else if (maze[i][j] == 'B' && (maze[i-1][j] == 'E' || maze[i-1][j] == 'P')){
			maze[i][j] = 'E';
			maze[i-1][j] = (maze[i-1][j] == 'E' ? 'B' : 'X');
			return true;
			
		}
		

		else if (maze[i][j] == 'X' && (maze[i-1][j] == 'E' || maze[i-1][j] == 'P')){
			maze[i][j] = 'P';
			maze[i-1][j] = (maze[i-1][j] == 'E' ? 'B' : 'X');
			return true;
		}
		
		
		return false;
	}

	public boolean moveDown(int i, int j) {
		if (maze[i][j] == 'W')
			return false;
		else if (maze[i][j] == 'E' || maze[i][j] == 'P')
			return true;
		
		else if (maze[i][j] == 'B' && (maze[i+1][j] == 'E' || maze[i+1][j] == 'P')){
			maze[i][j] = 'E';
			maze[i+1][j] = (maze[i+1][j] == 'E' ? 'B' : 'X');
			return true;
			
		}
		

		else if (maze[i][j] == 'X' && (maze[i+1][j] == 'E' || maze[i+1][j] == 'P')){
			maze[i][j] = 'P';
			maze[i+1][j] = (maze[i+1][j] == 'E' ? 'B' : 'X');
			return true;
		}
		
		
		return false;
	}

	public void change(Scanner scan) throws Exception {
		height = Integer.parseInt(scan.nextLine());
		width = Integer.parseInt(scan.nextLine());
		if (height < 1 || width < 1){
			throw new Exception ("Incorrect size of maze");
		}
		
		robotRow = Integer.parseInt(scan.nextLine());
		robotCol = Integer.parseInt(scan.nextLine());
		if (robotRow < 1 || robotCol < 1){
			throw new Exception ("Incorrect robot's coordinates");
		}
		
		
		maze = new char [height][width];
		
		for (int i = 0; i < height; i++){
			String s = scan.nextLine();
			if (s.length() != width){
				throw new Exception ("Incorrect maze's data");
			}			
			for (int j = 0; j < width; j++){
				maze[i][j] = s.charAt(j);
				if (maze[i][j] != 'E' && maze[i][j] != 'W' && maze[i][j] != 'B' && maze[i][j] != 'P' && maze[i][j] != 'R'){
					throw new Exception ("Incorrect maze's data");					
				}
			}
			
		}
		
		bestSteps = Integer.parseInt(scan.nextLine());
		if (bestSteps < 0){
			throw new Exception ("Incorrect value of number of best steps");
		}
		
		if (scan.hasNextLine()){
			throw new Exception ("Incorrect maze's data");
		}
		
	}

	
}
