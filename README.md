Implementation of the popular Sokoban game.
The goal of the game is to put all boxes to red points.
Main class - Field.

Arrows - move Sokoban.
U - undo last move (user can undo unlimited amount of times).
F1 - frame with information.