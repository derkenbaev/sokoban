import java.util.ArrayList;

public class Robot {
	private int row;
	private int col;
	
	ArrayList<Integer> rows = new ArrayList<Integer> ();
	ArrayList<Integer> cols = new ArrayList<Integer> ();
	
	
	public Robot (int row, int col){
		this.row = row;
		this.col = col;
		rows.add(row);
		cols.add(col);
	}
	
	
	public void moveUp(){
		if (row > 0){
			row--;
			rows.add(row);
			cols.add(col);
			
		}
	}
	
	

	public void moveDown(){
		if (row < 7){
			row++;
			rows.add(row);
			cols.add(col);
		}
	}
	
	
	public void moveRight(){
		if (col < 7){
			col++;
			rows.add(row);
			cols.add(col);
		}
	}
	

	public void moveLeft(){
		if (col > 0){
			col--;
			rows.add(row);
			cols.add(col);
			
		
		}
	}


	public void moveBack(){
		if (rows.size() > 1){
			rows.remove(rows.size() - 1);
			cols.remove(cols.size() - 1);
			row = rows.get(rows.size() - 1);
			col = cols.get(cols.size() - 1);
		}
	}


	public int getCol() {
		return col;
	}
	
	public int getRow() {
		return row;
	}


	public void change(Level level) {
		row = level.getRobotRow();
		col = level.getRobotCol();
		rows = new ArrayList<Integer>();
		cols = new ArrayList<Integer>();
		rows.add(row);
		cols.add(col);
	}
}



