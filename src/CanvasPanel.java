import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class CanvasPanel extends JPanel {

	Robot r;
	Level level;

	CanvasPanel(Robot r, Level level) {
		this.r = r;
		this.level = level;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int h = getHeight();
		int w = getWidth();
		int dw = w / level.getWidth();
		int dh = h / level.getHeight();

		g.setColor(Color.BLACK);

		for (int i = 0; i <level.getWidth(); i++) {
			for (int j = 0; j < level.getHeight(); j++) {
				if (level.getChar(j, i) == 'W')
					g.setColor(new Color(156, 93, 82));
				else if (level.getChar(j, i) == 'B') {
					g.setColor(Color.YELLOW);
				} else if (level.getChar(j, i) == 'X') {
					g.setColor(Color.RED);
				}

				else {
					g.setColor(Color.WHITE);

				}
				g.fillRect(dw * i, dh * j, dw, dh);

				g.setColor(Color.BLACK);
				g.drawRect(dw * i, dh * j, dw, dh);

				if (level.getChar(j, i) == 'P') {
					g.setColor(Color.RED);
					g.drawRect(dw * i + 45, dh * j + 45, 10, 10);
					g.fillRect(dw * i + 45, dh * j + 45, 10, 10);

				}

			}
		}
		
		g.setColor(Color.BLUE);

		g.fillOval(r.getCol() * dw + dw / 4, r.getRow() * dh + dh / 4, dw / 2, dh / 2);
		
	}

}